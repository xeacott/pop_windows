import os


async def load_cpu(hub):
    return True
    # '''
    # Return some CPU information on Windows minions
    # '''
    # # Provides:
    # #   num_cpus
    # #   cpu_model
    # if 'NUMBER_OF_PROCESSORS' in os.environ:
    #     # Cast to int so that the logic isn't broken when used as a
    #     # conditional in templating. Also follows _linux_cpudata()
    #     try:
    #         hub.grains.GRAINS['num_cpus'] = int(os.environ['NUMBER_OF_PROCESSORS'])
    #     except ValueError:
    #         hub.grains.GRAINS['num_cpus'] = 1
    # hub.grains.GRAINS['cpu_model'] = hub.exec.reg.read_value(
    #     hive="HKEY_LOCAL_MACHINE",
    #     key="HARDWARE\\DESCRIPTION\\System\\CentralProcessor\\0",
    #     vname="ProcessorNameString").get('vdata')



# async def load_cpu(hub):
#     '''
#     Return some CPU information on Windows minions
#     '''
#     # Provides:
#     #   num_cpus
#     #   cpu_model
#     if 'NUMBER_OF_PROCESSORS' in os.environ:
#         # Cast to int so that the logic isn't broken when used as a
#         # conditional in templating. Also follows _linux_cpudata()
#         try:
#             hub.grains.GRAINS['num_cpus'] = int(os.environ['NUMBER_OF_PROCESSORS'])
#         except ValueError:
#             hub.grains.GRAINS['num_cpus'] = 1
#     hub.grains.GRAINS['cpu_model'] = hub.exec.reg.read_value(
#         hive="HKEY_LOCAL_MACHINE",
#         key="HARDWARE\\DESCRIPTION\\System\\CentralProcessor\\0",
#         vname="ProcessorNameString").get('vdata')

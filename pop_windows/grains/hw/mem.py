import win32api


async def memdata(hub):
    '''
    Return memory information.
    '''
    # get the Total Physical memory as reported by msinfo32
    tot_bytes = win32api.GlobalMemoryStatusEx()['TotalPhys']
    hub.grains.GRAINS['mem_total'] = int(tot_bytes / (1024 ** 2))

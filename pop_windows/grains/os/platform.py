import wmi


async def load_bios_info(hub):
    # http://msdn.microsoft.com/en-us/library/windows/desktop/aa394077(v=vs.85).aspx
    wmi_c = wmi.WMI()
    biosinfo = wmi_c.Win32_BIOS()[0]
    hub.grains.GRAINS['biosversion'] = biosinfo.Name.strip()

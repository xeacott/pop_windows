import sys

async def is_windows(hub):
    '''
    Verify that POP windows is running on windows.
    '''
    if not sys.platform.startswith('win'):
        raise OSError('POP-windows only runs on the windows kernel')
    return True

